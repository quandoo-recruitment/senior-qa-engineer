# Senior QA Engineer

Hello and thank you for applying for this position!

With this task, we would like to get an idea of how you approach UI and API test automation.

---

### Task Description:

Build a test automation framework:
- UI task: 
    - Design a test suite for login functionality on http://the-internet.herokuapp.com/login (existing user credentials are displayed on the page);

- API task: 
    - Go to https://reqres.in
    - Get user credentials from `GET` single user endpoint request
    - Using the credentials implement an E2E scenario for registration & login and verify that every endpoint returns the correct token.

---

### Goals:
- Working automated test suite
- `README.md` describing the following points:
    - Selected technology stack
    - Reasons behind the chosen framework and pattern(s)
    - How to make the framework work and how to execute the test(s)
    - Next possible steps for improvements

---

Once you’re done please send us the link to a Github/Bitbucket/Gitlab repo (***no zip files will be accepted*** 🛑) with your work.

We know you are busy, so we don’t want to set a strict timeline, but we would much appreciate, if you spend no more than a week.
Our main desire is to see how you think, so please concentrate on quality of your solution rather than on quantity.

If something isn’t clear, just send us an email with your questions! We are willing to help you succeed with the task 😊 

And last, but not least - have fun with it! Good luck 🤞
